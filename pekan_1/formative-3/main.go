package main

import (
	"fmt"
	"strconv"
)

func main() {
	// soal 1
	soalSatu()
	// soal 2
	soalKedua()
	// soal 3
	soalKetiga()
	// soal 4
	soalKeempat()
}

func soalSatu() {
	panjangPersegiPanjangString := "8"
	lebarPersegiPanjangString := "5"
	alasSegitigaString := "6"
	tinggiSegitigaString := "7"
	panjangPersegiPanjang, _ := strconv.Atoi(panjangPersegiPanjangString)
	lebarPersegiPanjang, _ := strconv.Atoi(lebarPersegiPanjangString)
	alasSegitiga, _ := strconv.Atoi(alasSegitigaString)
	tinggiSegitiga, _ := strconv.Atoi(tinggiSegitigaString)
	luasPersegiPanjang := panjangPersegiPanjang * lebarPersegiPanjang
	kelilingPersegiPanjang := 2 * (panjangPersegiPanjang + lebarPersegiPanjang)
	luasSegitiga := 0.5 * float64(alasSegitiga) * float64(tinggiSegitiga)
	fmt.Printf("Luas persegi panjang: %d\n", luasPersegiPanjang)
	fmt.Printf("Keliling persegi panjang: %d\n", kelilingPersegiPanjang)
	fmt.Printf("Luas segitiga: %f\n", luasSegitiga)
}

func soalKedua() {

	var nilaiJohn = 80
	var nilaiDoe = 50
	var indeksJohn string
	if nilaiJohn >= 80 {
		indeksJohn = "A"
	} else if nilaiJohn >= 70 && nilaiJohn < 80 {
		indeksJohn = "B"
	} else if nilaiJohn >= 60 && nilaiJohn < 70 {
		indeksJohn = "C"
	} else if nilaiJohn >= 50 && nilaiJohn < 60 {
		indeksJohn = "D"
	} else {
		indeksJohn = "E"
	}
	var indeksDoe string
	if nilaiDoe >= 80 {
		indeksDoe = "A"
	} else if nilaiDoe >= 70 && nilaiDoe < 80 {
		indeksDoe = "B"
	} else if nilaiDoe >= 60 && nilaiDoe < 70 {
		indeksDoe = "C"
	} else if nilaiDoe >= 50 && nilaiDoe < 60 {
		indeksDoe = "D"
	} else {
		indeksDoe = "E"
	}
	fmt.Println("Indeks nilai John:", indeksJohn)
	fmt.Println("Indeks nilai Doe:", indeksDoe)
}

func soalKetiga() {
	var tanggal = 30
	var bulan = 12
	var tahun = 1990
	fmt.Print("Tanggal lahir: ")
	fmt.Printf("%d ", tanggal)
	switch bulan {
	case 1:
		fmt.Print("Januari")
	case 2:
		fmt.Print("Februari")
	case 3:
		fmt.Print("Maret")
	case 4:
		fmt.Print("April")
	case 5:
		fmt.Print("Mei")
	case 6:
		fmt.Print("Juni")
	case 7:
		fmt.Print("Juli")
	case 8:
		fmt.Print("Agustus")
	case 9:
		fmt.Print("September")
	case 10:
		fmt.Print("Oktober")
	case 11:
		fmt.Print("November")
	case 12:
		fmt.Print("Desember")
	default:
		fmt.Print("Bulan tidak valid")
	}

	fmt.Printf(" %d\n", tahun)
}

func soalKeempat() {
	var tahunKelahiran = 1990
	var generasi string
	switch {
	case tahunKelahiran >= 1944 && tahunKelahiran <= 1964:
		generasi = "Baby Boomer"
	case tahunKelahiran >= 1965 && tahunKelahiran <= 1979:
		generasi = "Generasi X"
	case tahunKelahiran >= 1980 && tahunKelahiran <= 1994:
		generasi = "Generasi Y (Millenials)"
	case tahunKelahiran >= 1995 && tahunKelahiran <= 2015:
		generasi = "Generasi Z"
	default:
		generasi = "Generasi tidak dikenal"
	}
	fmt.Printf("Anda termasuk dalam generasi: %s\n", generasi)
}
