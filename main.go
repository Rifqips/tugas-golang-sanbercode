package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {

	// Tugas satu
	tugasSatu("Rifqi Padi Siliwangi,", "Jakarta, 27-06-00,", "Citayam, Depok, Jawa Barat")

	// Tugas dua
	tugasDua()

}

func tugasSatu(nama, ttl, alamat string) {
	fmt.Println(nama, ttl, alamat)
}

func tugasDua() {
	// soal 1
	const (
		pharse_one   = "Bootcamp"
		pharse_two   = "Digital"
		pharse_three = "Skill"
		pharse_four  = "Sanbercode"
		pharse_five  = "Golang"
	)
	fmt.Println(pharse_one, pharse_two, pharse_three, pharse_four, pharse_five)

	// soal 2
	halo := func(name string) string {
		if strings.Contains(name, "Halo Dunia") {
			return strings.Replace(name, "Dunia", "Golang", -1)
		}
		return name
	}
	fmt.Println(halo("Halo Dunia"))

	// soal 3
	const (
		kataPertama = "saya"
	)
	var kataKedua = "senang"
	var kataKetiga = "belajar"
	var kataKeempat = "golang"

	kataKedua = strings.ToUpper(string(kataKedua[0])) + kataKedua[1:] // mengambil huruf pertama sampai akhir
	kataKetiga = kataKetiga[0:6] + strings.ToUpper(string(kataKetiga[6]))
	kataKeempat = strings.ToUpper(kataKeempat)

	fmt.Println(kataPertama, kataKedua, kataKetiga, kataKeempat)

	// soal 4
	const (
		angkaPertama = "8"
		angkaKedua   = "5"
		angkaKetiga  = "6"
		angkaKeempat = "7"
	)

	total, err := convertAndSum(angkaPertama, angkaKedua, angkaKetiga, angkaKeempat)
	if err != nil {
		fmt.Println("Error:", err)
	} else {
		fmt.Println(total)
	}

	// soal 5
	kalimat := "halo halo bandung "
	var tahun = 2021
	year := strconv.Itoa(tahun)

	bandung := func(name string) string {
		if strings.Contains(name, kalimat) {
			return strings.Replace(name, "halo halo", "hi hi", -1)
		}
		return name
	}
	fmt.Println(bandung(kalimat + year))
}

func convertAndSum(nums ...string) (int, error) {
	total := 0
	for _, num := range nums {
		n, err := strconv.Atoi(num)
		if err != nil {
			return 0, err
		}
		total += n
	}
	return total, nil
}
